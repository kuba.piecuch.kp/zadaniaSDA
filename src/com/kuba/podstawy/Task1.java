package com.kuba.podstawy;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float circleDiameter = scanner.nextFloat();

        // float circuit = circleDiameter * 3.14f;
        float pi = (float) Math.PI;
        System.out.println(2 * circleDiameter * pi);
    }
}
